import {Component} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Scrap';

  // ng serve --proxy-config proxy.local.conf.json
  constructor(public router: Router) {
    this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          gtag('config', 'UA-162782419-1',
            {
              'page_path': event.urlAfterRedirects
            }
          );
        }
      }
    );
  }
}
