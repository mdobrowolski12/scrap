import {Component, OnInit} from '@angular/core';
import {FinancialService} from '../services/financial.service';
import {zip} from 'rxjs';
declare var zingchart: any;
@Component({
  selector: 'app-cards-holder',
  templateUrl: './cards-holder.component.html',
  styleUrls: ['./cards-holder.component.scss']
})
export class CardsHolderComponent implements OnInit {
  constructor(private financialService: FinancialService) {
  }
  public t: any[] = [];
  public v: any[] = [];
  public net: any[] = [];
  public btcV: any[] = [];
  charConfig = {
    "type": 'line',
    legend: {
      layout: "1x2",
    },
    "scale-x": {
      "min-value": 1279324800000,
      "step": "day",
      "transform": {
        "type": "date",
        "all": "%d %M %Y"
      },
      zooming: true
    },
    "scale-y": {
      guide: {
        visible: false,
      },
      "progression": "log",
      "log-base": 10,
      "item": {
        "font-size": 10
      },
      "autoFit":true
    },
    "scale-y-2": {
      "guide": {
        "line-style": "solid"
      },
      "item": {
        "font-size": 10
      },
      "autoFit":true
    },
    "series": [{
      "scales": "scale-x, scale-y",
      "values": [
      ]
    }, {
      "scales": "scale-x, scale-y-2",
      "values": [
      ]
    }],
    "crosshairX": {
      "plotLabel": {
        "text": '%t was %v<br>on %kl',
        "padding": '8px',
        "borderRadius": '5px'
      }
    },
  preview: {
    adjustLayout: true,
    borderWidth: '1px',
    handle: {
      height: '20px',
      lineWidth: '0px'
    }
  }
  };
  netConfig = {
    "type": 'line',
    legend: {
      layout: "1x2",
    },
    "scale-x": {
      "min-value": 1279324800000,
      "step": "day",
      "transform": {
        "type": "date",
        "all": "%d %M %Y"
      },
      zooming: true
    },
    "scale-y": {
      "progression": "log",
      "log-base": 10,
      guide: {
        visible: false,
      },
      "item": {
        "font-size": 10
      },
      "autoFit":true,
    },
    "scale-y-2": {
      "guide": {
        "line-style": "solid",
      },
      "item": {
        "font-size": 10
      },
      "autoFit":true
    },
    "series": [{
      "scales": "scale-x, scale-y",
      "values": [
      ],
    }, {
      "scales": "scale-x, scale-y-2",
      "values": [
      ]
    }],
    "crosshairX": {
      "plotLabel": {
        "text": '%t was %v<br>on %kl',
        "padding": '8px',
        "borderRadius": '5px'
      }
    },
    preview: {
      adjustLayout: true,
      borderWidth: '1px',
      handle: {
        height: '20px',
        lineWidth: '0px'
      }
    },
  };
  ngOnInit(): void {
    zip(
      this.financialService.getBtcPriceData(),
      this.financialService.getRelativeUnrealizedProfit(),
      this.financialService.getNetUnrealizedProfitLoss()
    ).subscribe(res => {
      this.net = res[2].map(values => values.v);
      this.v = res[1].map(values => values.v);
      this.btcV = res[0].map(values => values.v);
      this.t = res[0].map(values => values.t);
    }, error1 => {}, () => {
      zingchart.exec('chartBTC', 'setseriesdata', {
        data: [
          {
            text: "Reletive profit",
            scales: "scale-x, scale-y-2",
            values: this.v
          }, {
            text: "BTC",
            'line-color': "#585858",
            scales: "scale-x, scale-y",
            values: this.btcV
          }
        ]
      });
      zingchart.exec('chartNET', 'setseriesdata', {
        data: [
          {
            text: "Net unrealized profit/loss",
            scales: "scale-x, scale-y-2",
            "rules": [
              {
                rule: "%v < 0",
                'line-color': "#d72323",
              },
              {
                rule: "%v > 0 && %v < 0.25",
                'line-color': "#ffa148",
              },
              {
                rule: "%v < 0.5 && %v > 0.25",
                'line-color': "#FFD700",
              },
              {
                rule: "%v > 0.5 && %v < 0.75",
                'line-color': "#00FF00",
              },
              {
                rule: "%v > 0.75",
                'line-color': "#0000FF",
              }
            ],
            values: this.net
          }, {
            text: "BTC",
            'line-color': "#585858",
            scales: "scale-x, scale-y",
            values: this.btcV
          }
        ]
      });
    });
    zingchart.render({
      id: 'chartBTC',
      data: this.charConfig
    });
    zingchart.render({
      id: 'chartNET',
      data: this.netConfig
    });
  }
}
