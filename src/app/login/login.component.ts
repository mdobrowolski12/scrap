import {Component, OnInit} from '@angular/core';
import {FinancialService} from '../services/financial.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor(private financialService: FinancialService) {
  }

  onClick(event: Event) {

  }

  ngOnInit(): void {
  }
  authViaGoogle() {
        console.log('Auth');
        this.financialService.oauthViaGoogle().subscribe();
  }
}
