import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpServiceInterface} from './interfaces';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FinancialService implements HttpServiceInterface {
  private appKey = '9ff7eb4d-95f4-493d-ad2a-6fe094905a5c';
  private relativeUnrealizedUrl = `${environment.SERVER_URL}/api/metrics/indicators/unrealized_profit?a=BTC&api_key=${this.appKey}`;
  private netUnrealizedUrl = `${environment.SERVER_URL}/api/metrics/indicators/net_unrealized_profit_loss?a=BTC&api_key=${this.appKey}`;
  private btcUrl = `${environment.SERVER_URL}/api/metrics/market/price_usd_close?a=BTC&i=24h&api_key=${this.appKey}`;
  private oauthViaGoogleUrl = `${environment.SERVER_URL}/oauth2/authorization/google`;

  constructor(private httpClient: HttpClient) {
  }

  getRelativeUnrealizedProfit(): Observable<any> {
    return this.httpClient.get(this.relativeUnrealizedUrl);
  }

  getBtcPriceData(): Observable<any> {
    return this.httpClient.get(this.btcUrl);
  }

  getNetUnrealizedProfitLoss(): Observable<any> {
    return this.httpClient.get(this.netUnrealizedUrl);
  }
  oauthViaGoogle(): Observable<any> {
    return this.httpClient.get(this.oauthViaGoogleUrl);
  }
}
