import {Observable} from 'rxjs';

export interface HttpServiceInterface {
  getRelativeUnrealizedProfit(params?: any): Observable<any>;

  getNetUnrealizedProfitLoss(params?: any): Observable<any>;

  getBtcPriceData(params?: any): Observable<any>;
}
